package com.company;

import java.lang.reflect.Method;

public class CommandListener {
    @Command(commandName = "привет",
            args = "",
            desc = "Будь культурным, поздаровайся",
            showInHelp = true,
            aliases = {"здравствуйте"})
    public void hello(String[] args){
        System.out.println("Ну привет");
    }

    @Command(commandName = "пока",
            args = "",
            desc = "Перед уходом говори пока",
            showInHelp = true,
            aliases = {"прощай"})
    public void bie(String[] args){
        System.out.println("Пока");
    }

    @Command(commandName = "помощь",
            args = "",
            desc = "Выводит список команд",
            aliases = {"help", "команды"})
    public void help(String[] args){
        StringBuilder sb = new StringBuilder("Список команд: \n");
        for (Method m : this.getClass().getDeclaredMethods())
        {
            if (m.isAnnotationPresent(Command.class))
            {
                Command com = m.getAnnotation(Command.class);
                if (com.showInHelp()) //Если нужно показывать команду в списке.
                {
                    sb.append("Бот ")
                            .append(com.commandName()).append(" ")
                            .append(com.args()).append(" - ")
                            .append(com.desc()).append("\n");
                }
            }
        }
        System.out.println(sb);
    }
}
