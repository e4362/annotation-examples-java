package com.company;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MessageListener {
    private static final Map<String, Method> commands = new HashMap<>();
    private static final CommandListener listener = new CommandListener();

    static {
        for (Method m : listener.getClass().getDeclaredMethods()){
            if (m.isAnnotationPresent(Command.class)){
                Command cmd = m.getAnnotation(Command.class);
                commands.put(cmd.commandName(), m);

                for (String s : cmd.aliases()){
                    commands.put(s, m);
                }
            }
        }
    }

    public void onMessageReceived(String message){
        message = message.toLowerCase();
        try{
            if (message.startsWith("бот")){
                String[] args = message.split(" ");
                String command = args[1];
                String[] nArgs = Arrays.copyOfRange(args, 2, args.length);
                Method m = commands.get(command);
                if (m == null){
                    System.out.println("Вы что-то хотели?");
                    return;
                }
                Command com = m.getAnnotation(Command.class);
                if (nArgs.length < com.minArgs()){
                    System.out.println("Мало аргументов");
                }
                if (nArgs.length > com.maxArgs()){
                    System.out.println("Много аргументов");
                }
                Object[] param = {nArgs};
                m.invoke(listener, param);
            }
        }
        catch (ArrayIndexOutOfBoundsException e){
            System.out.println("Вы просто написали Бот");
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
